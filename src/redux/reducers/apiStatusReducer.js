import * as types from "../actions/actionTypes";
import initialState from "./initialState";

// Helper function determine when the action type end with 'SUCCESS' by using subString:
function actoinTypeEndsInSuccess(type) {
  return type.substring(type.length - 8) === "_SUCCESS";
}

// API STATUS REDUCER:
export default function apiCallStatusReducer(
  state = initialState.apiCallsInProgress,
  action
) {
  if (action.type === types.BEGIN_API_CALL) {
    return state + 1;
  } else if (
    action.type === types.API_CALL_ERROR ||
    actoinTypeEndsInSuccess(action.type)
  ) {
    return state - 1;
  }
  return state;
}
