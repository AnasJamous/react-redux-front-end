import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers"; // By Default It will import index.js
import reduxImmutableStateInvariant from "redux-immutable-state-invariant";
import thunk from "redux-thunk";

export default function configureStore(initialState) {
  // Add support for Redux dev tools:
  const composeEnhancer =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  return createStore(
    rootReducer,
    initialState,
    composeEnhancer(applyMiddleware(thunk, reduxImmutableStateInvariant()))
  );
}

// Redux middleware is a way to enhance redux's behavior
// reduxImmutableStateInvariant() function is useful to warn us if we accidentally mutate redux state
