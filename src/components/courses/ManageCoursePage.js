import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import * as courseActions from "../../redux/actions/courseAction";
import * as authorActions from "../../redux/actions/authorActions";
import PropTypes, { object } from "prop-types";
import CourseForm from "./CourseForm";
import { newCourse } from "../../../tools/mockData";
import { toast } from "react-toastify";

function ManageCoursePage({
  courses,
  authors,
  saveCourse,
  loadAuthors,
  loadCourses,
  history,
  ...props
}) {
  // NOTE: we are using local state rather than putting it in redux store
  // because this state no other components are care about it if it changed
  //
  const [course, setCourse] = useState({ ...props.course });
  const [errors, setErrors] = useState({});
  const [saving, setSaving] = useState(false);

  useEffect(() => {
    if (courses.length === 0) {
      loadCourses().catch((error) => {
        alert("Loading courses failed" + error);
      });
    } else {
      setCourse({ ...props.course });
    }
    if (authors.length === 0) {
      loadAuthors().catch((error) => {
        alert("Loading Authors failed" + error);
      });
    }
  }, [props.course]);

  function handleChange(event) {
    console.log("event from on change event in course form component", event);
    const { name, value } = event.target;
    // using parasInt to convert an Id back to an int:
    setCourse((prevCourse) => ({
      ...prevCourse,
      [name]: name === "authorId" ? parseInt(value, 10) : value,
    }));
  }

  function formIsValid() {
    const { title, authorId, category } = course;
    const errors = {};

    if (!title) errors.title = "Title is required";
    if (!authorId) errors.author = "Author is required";
    if (!category) errors.category = "Category is required";

    setErrors(errors);
    // Form is valid if the errors object still has no properties:
    return Object.keys(errors).length === 0;
  }
  function handleSave(event) {
    event.preventDefault();
    if (!formIsValid()) return;
    setSaving(true);
    saveCourse(course)
      .then(() => {
        toast.success("Course saved.");
        history.push("/courses");
      })
      .catch((error) => {
        setSaving(false);
        setErrors({ onSave: error.message });
      });
  }

  return (
    <CourseForm
      course={course}
      errors={errors}
      authors={authors}
      onSave={handleSave}
      onChange={handleChange}
      saving={saving}
    />
  );
}

// PropTypes helps us specify the props that our component exepts:
// PropTypes declerations:
ManageCoursePage.propTypes = {
  course: PropTypes.object.isRequired,
  courses: PropTypes.array.isRequired,
  authors: PropTypes.array.isRequired,
  loadAuthors: PropTypes.func.isRequired,
  loadCourses: PropTypes.func.isRequired,
  saveCourse: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

export function getCourseBySlug(courses, slug) {
  return courses.find((course) => course.slug === slug) || null;
}

// This function determines what state is passed to our component via props:

// NOTE: to get parameter from the url this is a handy paramater we
// can pass to the mapStateToProps function is called ownProps
function mapStateToProps(state, ownProps) {
  const slug = ownProps.match.params.slug;
  const _newCourse =
    slug && state.courses.length > 0
      ? getCourseBySlug(state.courses, slug)
      : newCourse;
  return {
    course: _newCourse,
    courses: state.courses,
    authors: state.authors,
  };
}

// This function let us declare what actions to pass to our component on props:
// This is another way to declare what actions is passed to component using object form:
// It's more clear ... for The another way reference check
//  CoursePage.js It is use the function way to pass an actions to the component.
const mapDispatchtoProps = {
  loadCourses: courseActions.loadCourses,
  loadAuthors: authorActions.loadAuthors,
  saveCourse: courseActions.saveCourse,
};

// Connect out component to redux:
const connectedStateAndProps = connect(mapStateToProps, mapDispatchtoProps);
export default connectedStateAndProps(ManageCoursePage);
